#-------------------------------------------------------------------------------
#
# CONFIGURATION FILE FOR MENTAT-IDEAGEN.PY MODULE
#
#-------------------------------------------------------------------------------

{
    #---------------------------------------------------------------------------
    # Custom script configurations
    #---------------------------------------------------------------------------

    # Number of messages to be generated in one batch. This configuration is
    # mutually exclusive with 'random_count' and always comes as a second.
    #   default: 1
    #   type:    integer
    #
    #"count": 1,

    # Upper limit for random number of messages to be generated in one batch.
    # This configuration is mutually exclusive with 'count' and takes precedence.
    #   default: None
    #   type:    integer
    #
    #"random-count": 10,

    # Generate messages continuously with given time backoff (flag).
    #   default: false
    #   type:    boolean
    #"steady": false,

    # Time back off between message batches.
    #   default: 60
    #   type:    integer
    #
    #"back_off": 60,

    # Name of the target queue directory.
    #   default: "/var/mentat/spool/mentat-inspector.py/incoming"
    #   type:    string
    #
    #"queue_dir": "/var/mentat/spool/mentat-inspector.py/incoming",

    # Name of the temporary file directory.
    #   default: "/var/mentat/spool/mentat-inspector.py/tmp"
    #   type:    string
    #
    #"temp_dir": "/var/mentat/spool/mentat-inspector.py/tmp",

    # Name of the template file for generating messages.
    #   default: "msg.01.idea.j2"
    #   type:    string
    #
    #"template": "msg.01.idea.j2",

    # Name of the directory containing message templates.
    #   default: "/etc/mentat/templates/idea"
    #   type:    string
    #
    #"template_dir": "/etc/mentat/templates/idea",

    #---------------------------------------------------------------------------
    # Configurations for IDEA message pseudo random generation.
    #---------------------------------------------------------------------------

    # List of IPv4 source adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_source_ip4s": [
        "192.168.0.1",
        "192.168.0.2",
        "192.168.0.3",
        "192.168.0.4",
        "192.168.0.5",
        "10.0.0.1",
        "10.0.0.2",
        "10.0.0.3",
        "10.0.0.4",
        "10.0.0.5",
        "195.113.144.234",
        "195.113.144.194",
        "195.113.144.201",
        "195.113.144.230",
        "78.128.211.97"
    ],

    # List of IPv4 target adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_target_ip4s": [
        "192.168.5.1",
        "192.168.5.2",
        "192.168.5.3",
        "192.168.5.4",
        "192.168.5.5",
        "10.10.0.1",
        "10.10.0.2",
        "10.10.0.3",
        "10.10.0.4",
        "10.10.0.5",
        "195.113.144.234",
        "195.113.144.194",
        "195.113.144.201",
        "195.113.144.230",
        "78.128.211.97"
    ],

    # List of IPv6 source adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_source_ip6s": [
        "2001:db8::ff01:42:0/112",
        "2001:db8::ff02:42:0/112",
        "2001:db8::ff03:42:0/112",
        "2001:db8::ff04:42:0/112",
        "2001:db8::ff05:42:0/112",
        "2001:718:1:a200::11:1",
        "2001:718:1:a200::11:2",
        "2001:718:1:a200::11:3",
        "2001:718:1:a200::11:4",
        "2001:718:1:a200::11:5",
        "2a00:1450:4014:801::2004",
        "2a00:1450:4014:801::2005",
        "2a00:1450:4014:801::2006",
        "2a00:1450:4014:801::2007",
        "2a00:1450:4014:801::2008"
    ],

    # List of IPv6 target adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_target_ip6s": [
        "1001:db8::ff01:42:0/112",
        "1001:db8::ff02:42:0/112",
        "1001:db8::ff03:42:0/112",
        "1001:db8::ff04:42:0/112",
        "1001:db8::ff05:42:0/112",
        "1001:718:1:a200::11:1",
        "1001:718:1:a200::11:2",
        "1001:718:1:a200::11:3",
        "1001:718:1:a200::11:4",
        "1001:718:1:a200::11:5",
        "1a00:1450:4014:801::2004",
        "1a00:1450:4014:801::2005",
        "1a00:1450:4014:801::2006",
        "1a00:1450:4014:801::2007",
        "1a00:1450:4014:801::2008"
    ],

    # List of source mac adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_source_macs": [
        "02:00:00:00:00:00",
        "02:00:00:00:00:01",
        "02:00:00:00:00:02",
        "02:00:00:00:00:03",
        "02:00:00:00:00:04",
        "02:00:00:00:00:05",
        "02:00:00:00:00:06"
    ],

    # List of target mac adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_target_macs": [
        "0A:00:00:00:00:00",
        "0A:00:00:00:00:01",
        "0A:00:00:00:00:02",
        "0A:00:00:00:00:03",
        "0A:00:00:00:00:04",
        "0A:00:00:00:00:05",
        "0A:00:00:00:00:06"
    ],

    # List of source hostname adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_source_hostnames": [
        "bogus-source1.com",
        "bogus-source2.com",
        "bogus-source3.com",
        "bogus-source4.com",
        "bogus-source5.com",
        "bogus-source6.com"
    ],

    # List of target hostname adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_target_hostnames": [
        "bogus-target1.com",
        "bogus-target2.com",
        "bogus-target3.com",
        "bogus-target4.com",
        "bogus-target5.com",
        "bogus-target6.com"
    ],

    # List of source url adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_source_urls": [
        "http://bogus-source1.com/aaa",
        "http://bogus-source2.com/bbb",
        "http://bogus-source3.com/ccc",
        "http://bogus-source4.com/ddd",
        "http://bogus-source5.com/eee",
        "http://bogus-source6.com/fff"
    ],

    # List of target url adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_target_urls": [
        "http://bogus-target1.com/aaa",
        "http://bogus-target2.com/bbb",
        "http://bogus-target3.com/ccc",
        "http://bogus-target4.com/ddd",
        "http://bogus-target5.com/eee",
        "http://bogus-target6.com/fff"
    ],

    # List of source email adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_source_emails": [
        "aaa@bogus-source1.com",
        "bbb@bogus-source2.com",
        "ccc@bogus-source3.com",
        "ddd@bogus-source4.com",
        "eee@bogus-source5.com",
        "fff@bogus-source6.com"
    ],

    # List of target email adresses for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_target_emails": [
        "aaa@bogus-target1.com",
        "bbb@bogus-target2.com",
        "ccc@bogus-target3.com",
        "ddd@bogus-target4.com",
        "eee@bogus-target5.com",
        "fff@bogus-target6.com"
    ],

    # List of node names for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_node_names": [
        "cz.cesnet.lister",
        "cz.cesnet.rimmer",
        "cz.cesnet.kryten",
        "cz.cesnet.cat",
        "cz.cesnet.holly",
        "cz.cesnet.queeg"
    ],

    # List of node software names for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_node_sws": [
        "Kippo",
        "Dionaea",
        "LaBrea",
        "Suricata",
        "Snort",
        "Firewall",
        "Fail2Ban",
        "HoneyBOT",
        "ShadowServer",
        "TippingPoint"
    ],

    # List of IDEA category names for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_categories": [
        "Abusive.Spam",
        "Anomaly.Traffic",
        "Attempt.Exploit",
        "Availability.DoS",
        "Availability.DDoS",
        "Intrusion.Botnet",
        "Malware.Virus",
        "Vulnerable.Config",
        "Vulnerable.Open",
        "Recon.Scanning"
    ],

    # List of IDEA protocol names for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_protocols": [
        "tcp",
        "udp",
        "ipv4",
        "ipv6",
        "icmp",
        "ssh",
        "http",
        "https",
        "irc",
        "ftp"
    ],

    # List of IDEA event severity names for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_severities": [
        "low",
        "medium",
        "high",
        "critical",
        null
    ],

    # List of IDEA event class names for random message generation.
    #   default: []
    #   type:    list of strings
    #
    "list_classes": [
        "event-class-01",
        "event-class-02",
        "event-class-03",
        "event-class-04",
        null
    ],

    # List of IDEA event source and target tags for random message generation.
    # Following list was created according to the official documentation at
    # https://idea.cesnet.cz/en/classifications.
    #   default: []
    #   type:    list of strings
    #
    "list_srctgt_tags": [
        "Proxy",
        "OriginMalware",
        "OriginSandbox",
        "OriginSpam",
        "Phishing",
        "Malware",
        "MITM",
        "Spam",
        "Backscatter",
        "Open",
        "Poisoned",
        "FastFlux",
        "Botnet",
        "CC"
    ],

    # List of IDEA event source and target tags for random message generation.
    # Following list was created according to the official documentation at
    # https://idea.cesnet.cz/en/classifications.
    #   default: []
    #   type:    list of strings
    #
    "list_node_tags": [
        "Connection",
        "Datagram",
        "Content",
        "Data",
        "File",
        "Flow",
        "Log",
        "Protocol",
        "Host",
        "Network",
        "Correlation",
        "External",
        "Reporting",
        "Relay",
        "Auth",
        "Blackhole",
        "Signature",
        "Statistical",
        "Heuristic",
        "Integrity",
        "Policy",
        "Honeypot",
        "Tarpit",
        "Recon",
        "Monitor"
    ],

    #---------------------------------------------------------------------------
    # Common script configurations
    #---------------------------------------------------------------------------

    #"regular": false,
    #"shell": false,
    #"command": "generate-random",
    #"interval": '5_minutes'
    #"adjust_thresholds": false,
    #"time_high": null,

    #---------------------------------------------------------------------------
    # Common application configurations
    #---------------------------------------------------------------------------

    #"quiet": false,
    #"verbosity": 0,
    #"log_file": "/var/mentat/log/mentat-ideagen.py.log",
    #"log_level": "info",
    #"runlog_dir": "/var/mentat/run/mentat-ideagen.py",
    #"runlog_dump": false,
    #"runlog_log": false,
    #"pstate_file": "/var/mentat/run/mentat-ideagen.py.pstate",
    #"pstate_dump": false,
    #"pstate_log": false,
    #"action": null,
    #"user": "mentat",
    #"group": "mentat",

    # This is a dummy last configuration so that the user does not have to fix
    # the commas in the whole configuration file after each change.
    "_dummy_": "_dummy_"
}
