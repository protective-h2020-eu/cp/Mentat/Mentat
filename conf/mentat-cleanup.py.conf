#-------------------------------------------------------------------------------
#
# CONFIGURATION FILE FOR MENTAT-CLEANUP.PY MODULE
#
#-------------------------------------------------------------------------------

{
    #---------------------------------------------------------------------------
    # Custom script configurations
    #---------------------------------------------------------------------------

    # Path to database files (for disk usage measurements).
    #   default: "/var/lib/postgresql"
    #   type:    string
    #
    "db_path": "/var/lib/postgresql",

    # Perform simulation, do not remove anything (flag).
    #   default: false
    #   type:    boolean
    #
    "simulate": false,

    # List of maintained collections.
    #   default: empty list
    #   type:    list of dicts
    #
    "events": [
        {
            "threshold_type": "y"
        },
        {
            "threshold_type": "8w",
            "filter_spec": { "groups": "__EMPTY__"}
        }
    ],

    # List of maintained database tables.
    #   default: empty list
    #   type:    list of dicts
    #
    "tables": [
        {
            "table": "enum_category",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_eventclass",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_eventseverity",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_inspectionerrors",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_resolvedabuses",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_node_name",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_node_type",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_protocol",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_source_type",
            "column": "last_seen",
            "threshold_type": "y"
        },
        {
            "table": "enum_target_type",
            "column": "last_seen",
            "threshold_type": "y"
        }
    ],

    # List of maintained cache folders.
    #   default: empty list
    #   type:    list of dicts
    #
    "caches": [
        {
            "cachedir": "/var/mentat/cache",
            "threshold_type": "w"
        },
        {
            "cachedir": "/var/mentat/charts",
            "threshold_type": "w"
        },
        {
            "cachedir": "/var/mentat/reports/reporter",
            "threshold_type": "y"
        }
    ],

    # List of maintained runlog folders.
    #   default: empty list
    #   type:    list of dicts
    #
    "runlogs": [
        {
            "runlogdir": "/var/mentat/run",
            "threshold_type": "4w"
        }
    ],

    #---------------------------------------------------------------------------
    # Common script configurations
    #---------------------------------------------------------------------------

    #"regular": false,
    #"shell": false,
    #"command": "cleanup",
    "interval": "hourly",
    #"adjust_thresholds": false,
    #"time_high": null,

    #---------------------------------------------------------------------------
    # Common application configurations
    #---------------------------------------------------------------------------

    #"quiet": false,
    #"verbosity": 0,
    #"log_file": "/var/mentat/log/mentat-cleanup.py.log",
    #"log_level": "info",
    #"runlog_dir": "/var/mentat/run/mentat-cleanup.py",
    #"runlog_dump": false,
    #"runlog_log": false,
    #"pstate_file": "/var/mentat/run/mentat-cleanup.py.pstate",
    #"pstate_dump": false,
    #"pstate_log": false,
    #"action": null,
    #"user": "mentat",
    #"group": "mentat",

    # This is a dummy last configuration so that the user does not have to fix
    # the commas in the whole configuration file after each change.
    "_dummy_": "_dummy_"
}
