#!/bin/bash
#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

cd /
sudo -u postgres psql -c "CREATE EXTENSION IF NOT EXISTS ip4r;"
sudo -u postgres psql -c "SELECT usename FROM pg_catalog.pg_user;" | grep mentat > /dev/null
if [ $? -ne 0 ]; then
    echo "Creating default PostgreSQL user 'mentat'"
    sudo -u postgres psql -c "CREATE USER mentat WITH PASSWORD 'mentat';"
fi


for dbname in "mentat_events" "mentat_main" "mentat_utest" "mentat_bench" "vial"
do
    sudo -u postgres psql -c "SELECT datname FROM pg_catalog.pg_database;" | grep $dbname > /dev/null
    if [ $? -ne 0 ]; then
        echo "Creating default PostgreSQL database '$dbname'"
        sudo -u postgres psql -c "CREATE DATABASE $dbname;"
        sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $dbname TO mentat;"
    fi
    sudo -u postgres psql -d "$dbname" -c "CREATE EXTENSION IF NOT EXISTS ip4r;"
done
