#!/bin/bash
#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

cd /

echo "Optimizing AUTOVACUUM for database 'mentat_events'"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events SET (autovacuum_vacuum_threshold=10000);"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events SET (autovacuum_vacuum_scale_factor=0.0);"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events_json SET (autovacuum_vacuum_threshold=10000);"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events_json SET (autovacuum_vacuum_scale_factor=0.0);"

echo "Optimizing AUTOANALYZE for database 'mentat_events'"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events SET (autovacuum_analyze_threshold=20000);"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events SET (autovacuum_analyze_scale_factor=0.0);"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events_json SET (autovacuum_analyze_threshold=20000);"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events_json SET (autovacuum_analyze_scale_factor=0.0);"

echo "Optimizing CLUSTERs for database 'mentat_events'"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events CLUSTER ON events_detecttime_idx;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events_json CLUSTER ON events_json_pkey;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE events_thresholded CLUSTER ON events_thresholded_pkey;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE thresholds CLUSTER ON thresholds_pkey;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_category CLUSTER ON enum_category_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_eventclass CLUSTER ON enum_eventclass_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_eventseverity CLUSTER ON enum_eventseverity_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_inspectionerrors CLUSTER ON enum_inspectionerrors_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_resolvedabuses CLUSTER ON enum_resolvedabuses_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_node_name CLUSTER ON enum_node_name_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_node_type CLUSTER ON enum_node_type_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_protocol CLUSTER ON enum_protocol_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_source_type CLUSTER ON enum_source_type_data_key;"
sudo -u postgres psql -d "mentat_events" -c "ALTER TABLE enum_target_type CLUSTER ON enum_target_type_data_key;"

echo "Optimizing CLUSTERs for database 'mentat_main'"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE users CLUSTER ON users_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE groups CLUSTER ON groups_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE networks CLUSTER ON networks_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE filters CLUSTER ON filters_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE reports_events CLUSTER ON reports_events_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE statistics_events CLUSTER ON statistics_events_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE changelogs_items CLUSTER ON changelogs_items_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE settings_reporting CLUSTER ON settings_reporting_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE asoc_group_managers CLUSTER ON asoc_group_managers_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE asoc_group_members CLUSTER ON asoc_group_members_pkey;"
sudo -u postgres psql -d "mentat_main" -c "ALTER TABLE asoc_group_members_wanted CLUSTER ON asoc_group_members_wanted_pkey;"
