#!/bin/bash
#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Author: Jan Mach <jan.mach@cesnet.cz>
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------


# Utility script for IDEA event database migrations.
#
# This script is a simple wrapper around `Alembic <https://alembic.sqlalchemy.org/en/latest/index.html>`__
# migration tool and is intended to be used on target systems where Mentat was
# installed from packages.


cwd=$(pwd)
cd /etc/mentat/migrations-events
alembic "$@"
cd $cwd
