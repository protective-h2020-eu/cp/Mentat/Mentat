#!/bin/bash
#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

. /etc/default/mentat
cd /

ADDRS=${@}

# In case list of report recipients is not given as command line argument, use
# the default list from /etc/default/mentat configuration file.
if [ -z "$ADDRS" ]
then
    ADDRS=${MENTAT_ADMINS_MAIL_TO[@]}
fi

MARK_BEGIN=$SECONDS

echo ""
echo "Starting database maintenance at: `date --rfc-3339=second`"
echo ""
echo "#==============================================================================#"
echo "| Stopping the Mentat system:                                                  |"
echo "#==============================================================================#"
echo "  Current time: `date --rfc-3339=second`"
echo ""
printf 'SetOutputFilter SUBSTITUTE;DEFLATE\nSubstitute "s/__MAINTENANCE_START__/%b/n"\nSubstitute "s/__MAINTENANCE_END__/%b/n"\n' "`date '+%F %R'`" "`date -d '+4 hour' '+%F %R'`" > /etc/mentat/apache/maintenance/.htaccess
a2enmod substitute
a2dissite site_mentat-ng.conf
a2ensite site_maintenance.conf
systemctl restart apache2
mentat-controller.py --command disable
mentat-controller.py --command stop
systemctl restart postgresql


echo ""
echo "#==============================================================================#"
echo "| Performing database maintenance:                                             |"
echo "#==============================================================================#"
echo "  Current time: `date --rfc-3339=second`"
for dbname in mentat_events mentat_main; do
	echo ""
	echo "+------------------------------------------------------------------------------+"
	echo "  Database '$dbname'"
	echo "+------------------------------------------------------------------------------+"
	echo ""
	echo " * VACUUM:"
	echo ""
	time psql "$dbname" -c 'VACUUM FULL VERBOSE;'
	echo ""
	echo " * CLUSTER:"
	echo ""
	time psql "$dbname" -c 'CLUSTER VERBOSE;'
	echo ""
	echo " * ANALYZE:"
	echo ""
	time psql "$dbname" -c 'ANALYZE VERBOSE;'
done


echo ""
echo "#==============================================================================#"
echo "| Starting the Mentat system:                                                  |"
echo "#==============================================================================#"
echo "  Current time: `date --rfc-3339=second`"
echo ""
systemctl restart postgresql
mentat-controller.py --command start
mentat-controller.py --command enable
a2dismod substitute
a2dissite site_maintenance.conf
a2ensite site_mentat-ng.conf
systemctl restart apache2

MARK_END=$SECONDS
DURATION=`expr $MARK_END - $MARK_BEGIN`

{ printf "Dear administrator,\n\ndatabase maintenance has just finished. The whole process took %qh %qm %qs. Please login and make sure everything is in order.\n\n---\nWith regards\n\nMentat maintenance script\n\n##### SYSTEM STATUS #####\n\n" "$(($DURATION / 3600))" "$(($DURATION / 60))" "$(($DURATION % 60))" && system-status; } | mail -s "Mentat: Database maintenance finished" ${ADDRS[@]}

echo ""
echo "#==============================================================================#"
echo ""
echo "  Finished database maintenance at: `date --rfc-3339=second`"
echo "  Time elapsed: $(($DURATION / 3600))h $(($DURATION / 60))m $(($DURATION % 60))s"
echo ""