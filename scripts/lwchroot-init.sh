#!/bin/bash
#-------------------------------------------------------------------------------
# Utility script for creating local lightweight chroot substructure.
#
# Copyright (C) since 2011 CESNET, z.s.p.o
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

mkdir -p ./chroot
mkdir -p ./chroot/etc
mkdir -p ./chroot/etc/cron.d
mkdir -p ./chroot/var
mkdir -p ./chroot/var/mentat/backups
mkdir -p ./chroot/var/mentat/cache
mkdir -p ./chroot/var/mentat/charts
mkdir -p ./chroot/var/mentat/log
mkdir -p ./chroot/var/mentat/reports
mkdir -p ./chroot/var/mentat/rrds
mkdir -p ./chroot/var/mentat/run
mkdir -p ./chroot/var/mentat/spool
mkdir -p ./chroot/usr/share/GeoIP

if [ ! -L ./chroot/etc/mentat ]; then
	ln -s $(realpath ./conf) $(realpath ./chroot/etc)/mentat;
fi

if [ ! -f .env ]; then
	echo "APP_ROOT_PATH=$(realpath ./chroot)" >> .env
	echo "FLASK_ENV=development" >> .env
	echo "FLASK_CONFIG=development" >> .env
	echo "FLASK_CONFIG_FILE=$(realpath ./hawat.local.conf)" >> .env
fi

if [ ! -L ./migrations-events/.env ]; then
	ln -s $(realpath ./.env) $(realpath ./migrations-events)/.env;
fi