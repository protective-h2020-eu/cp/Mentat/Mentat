#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------


"""
Utility script for checking presence of various Python3 library dependencies.
"""


__author__  = "Jan Mach <jan.mach@cesnet.cz>"


import sys
import argparse
import importlib


VERBOSE = 0


def verbose_print(msg, level = 1):
    """
    Print given message, but only in case global ``VERBOSE`` flag is set.
    """
    global VERBOSE  # pylint: disable=locally-disabled,global-statement
    if VERBOSE >= level:
        print(msg)


if __name__ == "__main__":

    PARSER = argparse.ArgumentParser(
        description = 'Check for presence of various Python3 library dependencies'
    )

    # Optional arguments.
    PARSER.add_argument('--verbose', '-v', default = 0, action = 'count', help = 'increase output verbosity')

    ARGS = PARSER.parse_args()

    #---------------------------------------------------------------------------

    VERBOSE = ARGS.verbose

    required = (
        'ply', 'pydgets', 'pyzenkit', 'pynspect', 'ipranges', 'typedcols',
        'idea', 'geoip2', 'flask', 'flask_login', 'flask_mail', 'flask_babel',
        'flask_principal', 'flask_wtf', 'flask_debugtoolbar'
    )
    present  = []
    missing  = []

    verbose_print("Checking system for required Python3 library dependencies:")
    for l in required:
        try:
            importlib.import_module(l)
            present.append(l)
            verbose_print("[SUCCESS] mentat-ng - Python3 library dependency OK:   '{}'".format(l))
        except ImportError:
            missing.append(l)
            verbose_print("[ ERROR ] mentat-ng - Python3 library dependency MISS: '{}'".format(l))

    if missing:
        print("\n*********************************************************************")
        print("   MENTAT-NG: PLEASE INSTALL MISSING PYTHON3 LIBRARY DEPENDENCIES")
        for l in missing:
            print("\t* {}".format(l))
        print("*********************************************************************\n")
        sys.exit(1)
    else:
        sys.exit(0)
