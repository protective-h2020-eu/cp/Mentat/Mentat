#!/bin/bash

WHOIS="whois -h whois.ripe.net"

if [ "A$1A" = "AA" ]
then

    echo "Invalid usage!!! Script expects AS number as argument"
    exit 1

else

    echo "Listing networks for autonoumous system AS$1:"
    echo ""

fi

$WHOIS -r -i origin "AS$1" | grep route: | awk '{ print $2 }' | while read net; do
    echo "$net"
    #echo '%' $WHOIS -xB "$net" 
    #echo $WHOIS -xB "$net" 
    #echo '%' $WHOIS -MB "$net" 
    #echo $WHOIS -MB "$net" 
done
