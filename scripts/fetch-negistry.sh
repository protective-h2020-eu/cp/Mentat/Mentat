#!/bin/bash
#-------------------------------------------------------------------------------
# Utility script for fetching database from CESNET's Negistry service.
#
# Copyright (C) since 2011 CESNET, z.s.p.o
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------


#
# Helper functions for displaying information of various severity to the user.
#
function error {
    /bin/echo -e "${RED}[ERROR] $1${NC}" 1>&2
    exit 1
}

function info {
    if [ $FLAG_QUIET != 'YES' ]; then
        /bin/echo "$1"
    fi
}

function success {
    if [ $FLAG_QUIET != 'YES' ]; then
        /bin/echo -e "${GREEN}[SUCCESS] $1${NC}"
    fi
    exit 0
}

function usage {
    /bin/echo ""
    /bin/echo "Utility script for fetching database from CESNET's Negistry service."
    /bin/echo ""
    /bin/echo "Available options:"
    /bin/echo "    -h | --help    display this help message and exit"
    /bin/echo "    -d | --devel   fetch whois database from development server"
    /bin/echo "    -q | --quiet   run in quiet mode, display only errors"
    /bin/echo "    -s | --stub    install stub file in case of download error"
    /bin/echo ""
    /bin/echo "    -t=file"
    /bin/echo "    --target=file  name of the target file (defaults to '/var/mentat/whois-negistry.json')"
    /bin/echo ""
}

#-------------------------------------------------------------------------------


#
# Color code definitions for colored terminal output.
# Source: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
#
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m'

#
# Define global variables.
#
FLAG_DEVEL=NO
FLAG_QUIET=NO
FLAG_STUB=NO
LINK="https://negistry.cesnet.cz/db/api/v1/negistry-ripe-cesnet-ipranges.json"
LINK_DEV="https://irgames.cesnet.cz/negistry/db/api/v1/negistry-ripe-cesnet-ipranges.json"
TARGET="/var/mentat/whois-negistry.json"


#-------------------------------------------------------------------------------


#
# Command line argument parsing.
# Source: https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
#
for i in "$@"
do
case $i in
    -h|--help)
    usage
    exit 0
    ;;
    -d|--devel)
    FLAG_DEVEL=YES
    shift # past argument=value
    ;;
    -q|--quiet)
    FLAG_QUIET=YES
    shift # past argument=value
    ;;
    -s|--stub)
    FLAG_STUB=YES
    shift # past argument=value
    ;;
    -t=*|--target=*)
    TARGET="${i#*=}"
    shift # past argument=value
    ;;
    *)
    usage
    error "Invalid usage, unknown command line option '$i'"
    ;;
esac
done


#-------------------------------------------------------------------------------


if [ $FLAG_DEVEL == 'YES' ]; then
    LINK=$LINK_DEV
fi
info "Using remote server '$LINK'"
info "Using local target file '$TARGET'"


#
# Change to root directory.
#
cd /

#
# Prefered method is curl.
#
if which curl > /dev/null
then
    curl -4 --silent --insecure --output $TARGET $LINK

#
# Use wget as fallback.
#
elif which wget > /dev/null; then
    wget -4 --no-verbose --no-check-certificate -O $TARGET $LINK

else
    error "Unable to download Negistry whois database, missing 'curl' or 'wget' utilities"
fi

#
# Check, that the download was successfull (target file must exist).
#
if [ ! -f "$TARGET" ]
then
    if [ $FLAG_STUB == 'YES' ]; then
        info "Download of Negistry whois database failed, installing empty stub file '$TARGET'"
        echo '{"__negistry_about__":"Stub negistry file"}' > "$TARGET"
    else
        error "Download of Negistry whois database failed, file '$TARGET' does not exist"
    fi
fi

#
# Check, that the download was successfull (target file must not be older than 60 s).
#
if [ "$(( $(date +"%s") - $(date -r $TARGET +%s) ))" -gt "60" ]
then
    error "Download of Negistry whois database failed, file '$TARGET' is too old to be downloaded recently (last update at `date -r $TARGET`)"
fi

success "Successfully downloaded Negistry whois database into file '$TARGET' at `date`"
