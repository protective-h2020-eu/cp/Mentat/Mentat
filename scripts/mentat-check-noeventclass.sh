#!/bin/bash
#-------------------------------------------------------------------------------
# Example utility script for Mentat database and event data sanity check:
#	Events that did not fit any of our classes
#
# Author: Pavel Kácha <ph@cesnet.cz>
# Contributions: Jan Mach <mek@cesnet.cz>
# Copyright (C) since 2011 CESNET, z.s.p.o
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

. /etc/default/mentat
cd /

# Check, that Mentat system is enabled.
if test "x$MENTAT_IS_ENABLED" != "xyes"; then
    exit 0
fi

DAYS=${1:?You must provide check time interval in days}
shift
ADDRS=${@}
CURRDATE=`date`

# In case list of report recipients is not given as command line argument, use
# the default list from /etc/default/mentat configuration file.
if [ -z "$ADDRS" ]
then
    ADDRS=${MENTAT_CHECKS_MAIL_TO[@]}
fi

#-------------------------------------------------------------------------------

sudo --user=postgres psql --dbname=mentat_events --expanded <<EOF | tee --append /var/mentat/log/mentat-check-noeventclass.sh.log | mail -s 'Mentat check: Detectors sending unclassified events' -a "From: Mentat Sanity Checker <mentat@`hostname --fqdn`>" -a "X-Mentat-Report-Class: sanity-check" -a "X-Mentat-Report-Type: check-noeventclass" ${ADDRS[@]}
\set QUIET 1
SET timezone TO 'utc';
\timing on
\unset QUIET
\echo Dear administrator,
\echo
\echo here is a list of detectors producing events not fitting any of the predefined classes in last $DAYS day(s):
\echo
SELECT
	node_name AS "Detector",
	'${MENTAT_HAWAT_URL}events/' || MAX(id) || '/show' AS "Example event",
	COUNT(*) AS "Count"
FROM
	events
WHERE
	(eventclass IS NULL OR eventclass = '')
	AND storagetime > localtimestamp - INTERVAL '$DAYS day'
GROUP BY
	node_name
ORDER BY
	node_name;
\set QUIET 1
\timing off
\unset QUIET
\echo ---
\echo With regards
\echo
\echo Concerned sanity checking script
\echo Generated at: $CURRDATE
\echo Mailed to: ${ADDRS[@]}
\echo
EOF
