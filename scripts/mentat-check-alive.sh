#!/bin/bash
#-------------------------------------------------------------------------------
# Example utility script for Mentat database and event data sanity check:
#	Detectors that are dead over 2 days (but seen last week)
#
# Author: Pavel Kácha <ph@cesnet.cz>
# Contributions: Jan Mach <mek@cesnet.cz>
# Copyright (C) since 2011 CESNET, z.s.p.o
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

. /etc/default/mentat
cd /

# Check, that Mentat system is enabled.
if test "x$MENTAT_IS_ENABLED" != "xyes"; then
    exit 0
fi

DAYS_SEEN=${1:?You must provide check time interval in days}
DAYS_DEAD=2
shift
ADDRS=${@}
CURRDATE=`date`

# In case list of report recipients is not given as command line argument, use
# the default list from /etc/default/mentat configuration file.
if [ -z "$ADDRS" ]
then
    ADDRS=${MENTAT_CHECKS_MAIL_TO[@]}
fi

#-------------------------------------------------------------------------------

sudo --user=postgres psql --dbname=mentat_events --expanded <<EOF | tee --append /var/mentat/log/mentat-check-alive.sh.log | mail -s 'Mentat check: Detectors that appear to be dead' -a "From: Mentat Sanity Checker <mentat@`hostname --fqdn`>" -a "X-Mentat-Report-Class: sanity-check" -a "X-Mentat-Report-Type: check-alive" ${ADDRS[@]}
\set QUIET 1
SET timezone TO 'utc';
\timing on
\unset QUIET
\echo Dear administrator,
\echo
\echo here is a list of detectors that have been seen in last $DAYS_SEEN day(s) but now appear to be dead for over $DAYS_DEAD day(s):
\echo
SELECT
	node_name AS "Detector",
	MAX(storagetime) AS "Last event"
FROM
	events
WHERE
	storagetime > LOCALTIMESTAMP - INTERVAL '$DAYS_SEEN day'
GROUP BY
	node_name
HAVING
	MAX(storagetime) < LOCALTIMESTAMP - INTERVAL '$DAYS_DEAD day';
\set QUIET 1
\timing off
\unset QUIET
\echo ---
\echo With regards
\echo
\echo Concerned sanity checking script
\echo Generated at: $CURRDATE
\echo Mailed to: ${ADDRS[@]}
\echo
EOF
