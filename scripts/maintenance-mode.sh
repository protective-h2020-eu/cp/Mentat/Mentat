#!/bin/bash
#-------------------------------------------------------------------------------
# Utility script for enabling/disabling maintenance mode.
#
# Copyright (C) since 2011 CESNET, z.s.p.o
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

. /etc/default/mentat
cd /

#-------------------------------------------------------------------------------

function usage() {
    /bin/echo ""
    /bin/echo "Utility script for turning maintenance mode ON/OFF."
    /bin/echo ""
    /bin/echo "Available options:"
    /bin/echo "    -h  | --help    display this help message and exit"
    /bin/echo "    on  | ON        turn maintenance mode ON"
    /bin/echo "    off | OFF       turn maintenance mode OFF"
    /bin/echo ""
}

function maintenance_on() {

    echo ""
    echo "Starting database maintenance at: `date --rfc-3339=second`"
    echo ""
    echo "#==============================================================================#"
    echo "| Stopping the Mentat system:                                                  |"
    echo "#==============================================================================#"
    echo "  Current time: `date --rfc-3339=second`"
    echo ""
    printf 'SetOutputFilter SUBSTITUTE;DEFLATE\nSubstitute "s/__MAINTENANCE_START__/%b/n"\nSubstitute "s/__MAINTENANCE_END__/%b/n"\n' "`date '+%F %R'`" "`date -d '+4 hour' '+%F %R'`" > /etc/mentat/apache/maintenance/.htaccess
    a2enmod substitute
    a2dissite site_mentat-ng.conf
    a2ensite site_maintenance.conf
    systemctl restart apache2
    mentat-controller.py --command disable
    mentat-controller.py --command stop
    systemctl restart postgresql
    exit 0
}

function maintenance_off() {

    echo ""
    echo "Stopping database maintenance at: `date --rfc-3339=second`"
    echo ""
    echo "#==============================================================================#"
    echo "| Starting the Mentat system:                                                  |"
    echo "#==============================================================================#"
    echo "  Current time: `date --rfc-3339=second`"
    echo ""
    systemctl restart postgresql
    mentat-controller.py --command start
    mentat-controller.py --command enable
    a2dismod substitute
    a2dissite site_maintenance.conf
    a2ensite site_mentat-ng.conf
    systemctl restart apache2
    exit 0
}

#-------------------------------------------------------------------------------

if [[ $# -lt 1 ]]; then
    usage
    exit 1
fi

#
# Command line argument parsing.
# Source: https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
#
for i in "$@"
do
case $i in
    -h|--help)
    usage
    exit 0
    ;;
    on|ON)
    maintenance_on
    shift # past argument=value
    ;;
    off|OFF)
    maintenance_off
    shift # past argument=value
    ;;
    *)
    usage
    echo "Invalid usage, unknown command line option '$i'"
    ;;
esac
done
