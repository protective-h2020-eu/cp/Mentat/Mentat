#!/bin/bash
#-------------------------------------------------------------------------------
# Example utility script for Mentat database and event data sanity check:
#	Clients sending non static Descriptions (dynamic text like IPs should go to Note)
#
# Author: Pavel Kácha <ph@cesnet.cz>
# Contributions: Jan Mach <mek@cesnet.cz>
# Copyright (C) since 2011 CESNET, z.s.p.o
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

. /etc/default/mentat
cd /

# Check, that Mentat system is enabled.
if test "x$MENTAT_IS_ENABLED" != "xyes"; then
    exit 0
fi

DAYS=${1:?You must provide check time interval in days}
shift
ADDRS=${@}
CURRDATE=`date`

# In case list of report recipients is not given as command line argument, use
# the default list from /etc/default/mentat configuration file.
if [ -z "$ADDRS" ]
then
    ADDRS=${MENTAT_CHECKS_MAIL_TO[@]}
fi

#-------------------------------------------------------------------------------

sudo --user=postgres psql --dbname=mentat_events --expanded <<EOF | tee --append /var/mentat/log/mentat-check-volatiledescription.sh.log | mail -s 'Mentat check: Detectors sending events with volatile descriptions' -a "From: Mentat Sanity Checker <mentat@`hostname --fqdn`>" -a "X-Mentat-Report-Class: sanity-check" -a "X-Mentat-Report-Type: check-volatiledescription" ${ADDRS[@]}
\set QUIET 1
SET timezone TO 'utc';
\timing on
\unset QUIET
\echo Dear administrator,
\echo
\echo here is a list of detectors producing events with volatile description (dynamic text like IPs should go to Note) in last $DAYS day(s):
\echo
SELECT
	node_name AS "Detector",
	category AS "Categories",
	MAX(description) as "Example description",
	'${MENTAT_HAWAT_URL}events/' || MAX(id) || '/show' AS "Example event",
	COUNT(*) AS "Count"
FROM (
	SELECT
		node_name, category, description, MAX(id) as id
	FROM
		events
	WHERE
		storagetime > localtimestamp - INTERVAL '$DAYS day'
	GROUP BY
		node_name, category, description
) AS subquery
GROUP BY
	node_name, category
HAVING
	COUNT(*) > 10
ORDER BY
	node_name, category;
\set QUIET 1
\timing off
\unset QUIET
\echo ---
\echo With regards
\echo
\echo Concerned sanity checking script
\echo Generated at: $CURRDATE
\echo Mailed to: ${ADDRS[@]}
\echo
EOF
