"""
Make data in all enumeration tables not nullable.

Revision ID: a9e28583cc3b
Revises:
Create Date: 2019-01-31 09:11:40.127445

"""
from alembic import op

# Revision identifiers, used by Alembic.
revision = 'a9e28583cc3b'  # pylint: disable=locally-disabled,invalid-name
down_revision = None       # pylint: disable=locally-disabled,invalid-name
branch_labels = None       # pylint: disable=locally-disabled,invalid-name
depends_on = None          # pylint: disable=locally-disabled,invalid-name

# List of all current enumeration tables.
ENUM_TABLES = (
    "enum_category",
    "enum_protocol",
    "enum_node_name",
    "enum_node_type",
    "enum_source_type",
    "enum_target_type",
    "enum_cesnet_resolvedabuses",
    "enum_cesnet_eventclass",
    "enum_cesnet_eventseverity",
    "enum_cesnet_inspectionerrors"
)

def upgrade():  # pylint: disable=locally-disabled,missing-docstring
    for table in ENUM_TABLES:
        op.execute(  # pylint: disable=locally-disabled,no-member
            "DELETE FROM {:s} WHERE data is NULL".format(table)
        )
        op.alter_column(table, 'data',      nullable = False)  # pylint: disable=locally-disabled,no-member
        op.alter_column(table, 'last_seen', nullable = False)  # pylint: disable=locally-disabled,no-member


def downgrade():  # pylint: disable=locally-disabled,missing-docstring
    for table in ENUM_TABLES:
        op.alter_column(table, 'data',      nullable = False)  # pylint: disable=locally-disabled,no-member
        op.alter_column(table, 'last_seen', nullable = False)  # pylint: disable=locally-disabled,no-member
