"""
Added support for aggregated address ranges to improve event search performance.

Revision ID: 3fb6b209a5cd
Revises: a9e28583cc3b
Create Date: 2019-07-19 14:19:08.559678

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3fb6b209a5cd'       # pylint: disable=locally-disabled,invalid-name
down_revision = 'a9e28583cc3b'  # pylint: disable=locally-disabled,invalid-name
branch_labels = None            # pylint: disable=locally-disabled,invalid-name
depends_on = None               # pylint: disable=locally-disabled,invalid-name


def upgrade():  # pylint: disable=locally-disabled,missing-docstring
    for col in ('source_ip_aggr_ip4', 'target_ip_aggr_ip4'):
        op.execute(  # pylint: disable=locally-disabled,no-member
            "ALTER TABLE events ADD COLUMN {:s} ip4r".format(col)
        )
    for col in ('source_ip_aggr_ip6', 'target_ip_aggr_ip6'):
        op.execute(  # pylint: disable=locally-disabled,no-member
            "ALTER TABLE events ADD COLUMN {:s} ip6r".format(col)
        )
    op.execute(  # pylint: disable=locally-disabled,no-member
        """CREATE OR REPLACE FUNCTION aggr_ip4(iprange[]) RETURNS ip4r
AS
$$
DECLARE
    retVal ip4r;
    r_ip iprange;
    r_ip4 ip4r;
    _min ip4;
    _max ip4;
BEGIN
    FOREACH r_ip IN ARRAY $1 LOOP
        IF family(r_ip) = 4 THEN
            r_ip4 = r_ip::ip4r;
            IF retVal IS NULL THEN
                retVal := r_ip4;
            ELSE
                _min := lower(retVal);
                _max := upper(retVal);
                IF _min > lower(r_ip4) THEN
                    _min := lower(r_ip4);
                END IF;
                IF _max < upper(r_ip4) THEN
                    _max := upper(r_ip4);
                END IF;
                retVal := ip4r(_min, _max);
            END IF;
        END IF;
    END LOOP;
RETURN retVal;
END;
$$
LANGUAGE plpgsql;"""
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        """CREATE OR REPLACE FUNCTION aggr_ip6(iprange[]) RETURNS ip6r
AS
$$
DECLARE
    retVal ip6r;
    r_ip iprange;
    r_ip6 ip6r;
    _min ip6;
    _max ip6;
BEGIN
    FOREACH r_ip IN ARRAY $1 LOOP
        IF family(r_ip) = 6 THEN
            r_ip6 = r_ip::ip6r;
            IF retVal IS NULL THEN
                retVal := r_ip6;
            ELSE
                _min := lower(retVal);
                _max := upper(retVal);
                IF _min > lower(r_ip6) THEN
                    _min := lower(r_ip6);
                END IF;
                IF _max < upper(r_ip6) THEN
                    _max := upper(r_ip6);
                END IF;
                retVal := ip6r(_min, _max);
            END IF;
        END IF;
    END LOOP;
RETURN retVal;
END;
$$
LANGUAGE plpgsql;"""
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "UPDATE events SET source_ip_aggr_ip4 = aggr_ip4(source_ip), source_ip_aggr_ip6 = aggr_ip6(source_ip), target_ip_aggr_ip4 = aggr_ip4(target_ip), target_ip_aggr_ip6 = aggr_ip6(target_ip);"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "CREATE INDEX events_ip_aggr_idx ON events USING GIST (source_ip_aggr_ip4, target_ip_aggr_ip4, source_ip_aggr_ip6, target_ip_aggr_ip6)"
    )

def downgrade():  # pylint: disable=locally-disabled,missing-docstring
    op.drop_column('events', 'source_ip_aggr_ip4')  # pylint: disable=locally-disabled,no-member
    op.drop_column('events', 'source_ip_aggr_ip6')  # pylint: disable=locally-disabled,no-member
    op.drop_column('events', 'target_ip_aggr_ip4')  # pylint: disable=locally-disabled,no-member
    op.drop_column('events', 'target_ip_aggr_ip6')  # pylint: disable=locally-disabled,no-member
    op.drop_index('events_ip_aggr_idx', 'events')   # pylint: disable=locally-disabled,no-member
