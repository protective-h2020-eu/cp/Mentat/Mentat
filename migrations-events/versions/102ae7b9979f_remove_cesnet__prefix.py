"""Remove cesnet_ prefix

Revision ID: 102ae7b9979f
Revises: 0f31c168a714
Create Date: 2020-12-18 21:44:21.869689

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '102ae7b9979f'       # pylint: disable=locally-disabled,invalid-name
down_revision = '0f31c168a714'  # pylint: disable=locally-disabled,invalid-name
branch_labels = None            # pylint: disable=locally-disabled,invalid-name
depends_on = None               # pylint: disable=locally-disabled,invalid-name


def upgrade():  # pylint: disable=locally-disabled,missing-docstring
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_cesnet_resolvedabuses RENAME TO enum_resolvedabuses"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_cesnet_eventclass RENAME TO enum_eventclass"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_cesnet_eventseverity RENAME TO enum_eventseverity"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_cesnet_inspectionerrors RENAME TO enum_inspectionerrors"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS events_cesnet_storagetime_idx RENAME TO events_storagetime_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS events_cesnet_eventseverity_idx RENAME TO events_eventseverity_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_resolvedabuses_lastseen_idx RENAME TO enum_resolvedabuses_lastseen_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_eventclass_lastseen_idx RENAME TO enum_eventclass_lastseen_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_eventseverity_lastseen_idx RENAME TO enum_eventseverity_lastseen_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_inspectionerrors_lastseen_idx RENAME TO enum_inspectionerrors_lastseen_idx"
    )

    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_eventclass_data_key RENAME TO enum_eventclass_data_key"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_resolvedabuses_data_key RENAME TO enum_resolvedabuses_data_key"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_eventseverity_data_key RENAME TO enum_eventseverity_data_key"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_cesnet_inspectionerrors_data_key RENAME TO enum_inspectionerrors_data_key"
    )

    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME cesnet_resolvedabuses TO resolvedabuses"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME cesnet_storagetime TO storagetime"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME cesnet_eventclass TO eventclass"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME cesnet_eventseverity TO eventseverity"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME cesnet_inspectionerrors TO inspectionerrors"
    )


def downgrade():  # pylint: disable=locally-disabled,missing-docstring
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_resolvedabuses RENAME TO enum_cesnet_resolvedabuses"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_eventclass RENAME TO enum_cesnet_eventclass"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_eventseverity RENAME TO enum_cesnet_eventseverity"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS enum_inspectionerrors RENAME TO enum_cesnet_inspectionerrors"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS events_storagetime_idx RENAME TO events_cesnet_storagetime_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS events_eventseverity_idx RENAME TO events_cesnet_eventseverity_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_resolvedabuses_lastseen_idx RENAME TO enum_cesnet_resolvedabuses_lastseen_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_eventclass_lastseen_idx RENAME TO enum_cesnet_eventclass_lastseen_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_eventseverity_lastseen_idx RENAME TO enum_cesnet_eventseverity_lastseen_idx"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_inspectionerrors_lastseen_idx RENAME TO enum_cesnet_inspectionerrors_lastseen_idx"
    )

    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_eventclass_data_key RENAME TO enum_cesnet_eventclass_data_key"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_resolvedabuses_data_key RENAME TO enum_cesnet_resolvedabuses_data_key"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_eventseverity_data_key RENAME TO enum_cesnet_eventseverity_data_key"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER INDEX IF EXISTS enum_inspectionerrors_data_key RENAME TO enum_cesnet_inspectionerrors_data_key"
    )

    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME resolvedabuses TO cesnet_resolvedabuses"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME storagetime TO cesnet_storagetime"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME eventclass TO cesnet_eventclass"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME eventseverity TO cesnet_eventseverity"
    )
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE IF EXISTS events RENAME inspectionerrors TO cesnet_inspectionerrors"
    )
