"""Split event column from events to separate table

Revision ID: 0f31c168a714
Revises: 3fb6b209a5cd
Create Date: 2019-11-06 10:24:06.856478

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0f31c168a714'       # pylint: disable=locally-disabled,invalid-name
down_revision = '3fb6b209a5cd'  # pylint: disable=locally-disabled,invalid-name
branch_labels = None            # pylint: disable=locally-disabled,invalid-name
depends_on = None               # pylint: disable=locally-disabled,invalid-name


def upgrade():  # pylint: disable=locally-disabled,missing-docstring
    # Create separate table for storing IDEA events serialized into JSON.
    op.execute(  # pylint: disable=locally-disabled,no-member
        "CREATE TABLE IF NOT EXISTS events_json(id text PRIMARY KEY REFERENCES events(id) ON DELETE CASCADE, event bytea NOT NULL)"
    )
    # Copy data from 'events' table to 'events_json' table.
    op.execute(  # pylint: disable=locally-disabled,no-member
        "INSERT INTO events_json(id, event) SELECT id, event FROM events"
    )
    # Drop now unnecessary column.
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE events DROP COLUMN IF EXISTS event"
    )


def downgrade():  # pylint: disable=locally-disabled,missing-docstring
    # Add back the 'event' column to 'events' table.
    op.execute(  # pylint: disable=locally-disabled,no-member
        "ALTER TABLE events ADD COLUMN IF NOT EXISTS event bytea"
    )
    # Copy data from 'events_json' table to 'events' table.
    op.execute(  # pylint: disable=locally-disabled,no-member
        "UPDATE events SET event = subquery.event FROM (SELECT id, event FROM events_json) AS subquery"
    )
    # Drop now unnecessary table.
    op.execute(  # pylint: disable=locally-disabled,no-member
        "DROP TABLE IF EXISTS events_json"
    )
