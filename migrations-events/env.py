#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

"""
This module contains environment setup for `Alembic-based <https://alembic.sqlalchemy.org/en/latest/index.html>`__
database migrations for IDEA event database ``mentat_events``. The access to this
database is normally provided by `psycopg2 <http://initd.org/psycopg/>`__ driver,
but for migrations we want to use the same tool as for migrations of metadata
database ``mentat_main``.
"""

from __future__ import with_statement

from logging.config import fileConfig
import logging

from sqlalchemy import engine_from_config, pool
from alembic import context

from pyzenkit.utils import load_dotenv_cwd, get_resource_path
from pyzenkit.jsonconf import config_load_dir

# Load Mentat core configurations to get at the database configuration.
load_dotenv_cwd()
CONFIG_MENTAT = config_load_dir(get_resource_path('etc/mentat/core'))

# This is the Alembic Config object, which provides access to the values within
# the .ini file in use.
CONFIG = context.config

# Interpret the config file for Python logging. This line sets up loggers basically.
fileConfig(CONFIG.config_file_name)
LOGGER = logging.getLogger('alembic.env')

# Construct the proper database URI from separate configuration keys (sadly the
# configuration is optimized to be used with psycopg2 driver, which does not
# support single connection URI string).
CONFIG.set_main_option(
    'sqlalchemy.url',
    'postgresql://{user:s}:{password:s}@{host:s}:{port:d}/{dbname:s}'.format(
        **CONFIG_MENTAT['__core__database']['eventstorage']
    )
)

# Add your model's MetaData object here for 'autogenerate' support. We will not
# use this feature here, because we do not use SQLAlchemy for accessing the event
# database, we only want to use the same database migration tool.
TARGET_METADATA = None

# Other values from the config, defined by the needs of env.py, can be acquired:
#   my_important_option = config.get_main_option("my_important_option")
#   ... etc.

def run_migrations_offline():
    """
    Run migrations in 'offline' mode.

    This configures the context with just a URL and not an Engine, though an
    Engine is acceptable here as well. By skipping the Engine creation we don't
    even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the script output.
    """
    url = CONFIG.get_main_option("sqlalchemy.url")
    context.configure(
        url = url,
        target_metadata = TARGET_METADATA,
        literal_binds = True
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """
    Run migrations in 'online' mode.

    In this scenario we need to create an Engine and associate a connection with
    the context.
    """
    connectable = engine_from_config(
        CONFIG.get_section(CONFIG.config_ini_section),
        prefix = "sqlalchemy.",
        poolclass = pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection = connection,
            target_metadata = TARGET_METADATA
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
