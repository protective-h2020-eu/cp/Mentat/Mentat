Mentat SIEM - README
================================================================================

For more information about this project please refer to following resources:

* `Official project webpage <https://mentat.cesnet.cz/en/index>`__
* `Official project documentation <https://alchemist.cesnet.cz/mentat/doc/production/html/manual.html>`__
* `Issue tracking system <https://homeproj.cesnet.cz/projects/mentat>`__
* `Code repository <https://homeproj.cesnet.cz/git/mentat-ng.git/>`__


Copyright
--------------------------------------------------------------------------------

| Copyright: (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
| Author: Jan Mach <jan.mach@cesnet.cz>
| Use of this package is governed by the MIT license, see LICENSE file.
|
