.. Mentat documentation master file, created by
   sphinx-quickstart on Fri Mar 18 16:40:28 2016.

Welcome to Mentat's documentation!
================================================================================

.. note::

    Please be aware, that this documentation is appropriate for following version:

    * version:      |bversion| (|bdate|)
    * distribution: |codename| (|suite|)
    * Git revision: |urlrevtt|_

.. ifconfig:: build_suite in ('unstable','testing')

    .. warning::

        This documentation was generated from unstable codebase |bversion| |bnumber|.
        You may instead wish to read the documentation for :urldoc:`latest production version <production>`.


Contents:
--------------------------------------------------------------------------------

.. toctree::
   :maxdepth: 2

   _doclib/architecture.rst
   _doclib/events.rst
   _doclib/installation.rst
   _doclib/upgrading.rst
   _doclib/migration.rst
   _doclib/quickstart.rst
   _doclib/administration.rst
   _doclib/reporting.rst
   _doclib/database.rst
   _doclib/bin.rst
   _doclib/hawat.rst
   _doclib/development.rst
   _doclib/api.rst


Useful links and references
--------------------------------------------------------------------------------

* `official project website <https://mentat.cesnet.cz/en/index>`__
* `project support and bug tracking system (Redmine) <https://homeproj.cesnet.cz/projects/mentat>`__
* `primary code repository (Git) <https://homeproj.cesnet.cz/git/mentat-ng.git/>`__
* `automated project build system (Alchemist) <https://alchemist.cesnet.cz/alchemist/>`__


Indices and tables
================================================================================

* :ref:`genindex`
* :ref:`modindex`
