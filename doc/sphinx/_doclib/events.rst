.. _section-events:

Events
================================================================================

The Mentat system uses the `IDEA <https://idea.cesnet.cz>`__ message format to
represent security events it handles. It is a JSON based text format designed
to be both readable by humans and easy to process by machines.


Custom data attributes
--------------------------------------------------------------------------------

The Mentat system adds several custom data attributes to official `IDEA <https://idea.cesnet.cz>`__
message format. All these new data attributes are contained within the ``_Mentat``
data attribute.


.. _section-events-class:

Classification
````````````````````````````````````````````````````````````````````````````````

* **Key name:** ``EventClass``
* **Datatype:** ``string``

Classification is an internal feature similar to ``Category``. It attempts to
classify events with different syntax and/or from different detectors, that
represent same class of event. For example bruteforce attack to SSH daemon can
be detected both by some kind of network analyzer, or by some kind of local agent
inspecting log files. Both of these detectors can report the event, but the
contents of the event will be different due to the different nature of the detectors.

Classification is calculated by the classification instance of :ref:`section-bin-mentat-inspector`
using predefined but customizable set of rules.

The main goal of the classification attempts is to group events of the same kind
to be later processed


.. _section-events-severity:

Severity
````````````````````````````````````````````````````````````````````````````````

* **Key name:** ``EventSeverity``
* **Datatype:** ``enum (low|medium|high|critical)``


.. _section-events-abuses:

Resolved abuses
````````````````````````````````````````````````````````````````````````````````

* **Key name:** ``ResolvedAbuses``
* **Datatype:** ``list of string``


.. _section-events-srcasn:

Source autonomous systems (ASNs)
````````````````````````````````````````````````````````````````````````````````

* **Key name:** ``SourceResolvedASN``
* **Datatype:** list of `integer <https://idea.cesnet.cz/en/definition#integer>`__


.. _section-events-srccountry:

Source countries (ASNs)
````````````````````````````````````````````````````````````````````````````````

* **Key name:** ``SourceResolvedCountry``
* **Datatype:** ``list of string``


.. _section-events-storagetime:

Storage time
````````````````````````````````````````````````````````````````````````````````

* **Key name:** ``StorageTime``
* **Datatype:** `timestamp <https://idea.cesnet.cz/en/definition#timestamp>`__
