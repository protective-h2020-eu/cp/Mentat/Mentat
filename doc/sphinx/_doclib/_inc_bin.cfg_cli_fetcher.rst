``--db-host host-name``
    Name of the host running the database.

    *Type:* ``string``, *default:* ``localhost``

``--db-port number``
    Port number for connecting to database.

    *Type:* ``integer``, *default:* ``27017``

``--db-timeout miliseconds``
    Socket timeout in miliseconds for communicating with database.

    *Type:* ``integer``, *default:* ``3600000``

``--database db-name``
    Name of the database to connect to.

    *Type:* ``string``

``--collection col-name``
    Name of the collection to connect to.

    *Type:* ``string``
