.. _section-hawat-plugin-hosts:

hosts
================================================================================

This pluggable module provides features related to real time dashboard calculations 
for `IDEA <https://idea.cesnet.cz/en/index>`__ events. This module is currently 
experimental, because the searching and statistical calculations can be very 
performance demanding.
