``--queue-in-dir``
    Name of the input queue directory.

    *Type:* ``string``

``--queue-out-dir``
    Name of the output queue directory.

    *Type:* ``string``, *default:* ``None``

``--queue-out-limit``
    Limit on the number of the files for the output queue directory.

    *Type:* ``integer``, *default:* ``10000``

``--queue-out-wait``
    Waiting time when the output queue limit is reached in seconds.

    *Type:* ``integer``, *default:* ``30``
