.. _section-upgrading:

Upgrading
================================================================================

This document describes system upgrade process from previous Mentat production
versions within the ``2.x`` series. For upgrading from ``0.4.20`` to ``2.x``
series please see the :ref:`section-migration` section.

.. warning::

    Prerequisite for upgrading is an existing and working :ref:`installation <section-installation>`
    of all Mentat system packages.


.. _section-upgrading-mentat:

Upgrading Mentat system
--------------------------------------------------------------------------------

Please proceed according to the following recipe to safelly upgrade your installation
to latest version:

.. code-block:: shell

    # Launch tmux or screen.
    tmux

    # Step 0: Activate maintenance mode in case the downtime will be noticable for users:
    # First update timestamps of maintenance start and maintenance end:
    $ vim /etc/mentat/apache/maintenance/.htaccess
    # Now bring the Mentat system web interface down and maintenance site up:
    $ a2enmod substitute
    $ a2dissite site_mentat-ng.conf
    $ a2ensite site_maintenance.conf
    $ systemctl restart apache2

    # Step 1: It is recommended to stop Mentat daemons and cronjobs before upgrade:
    $ mentat-controller.py --command stop
    $ mentat-controller.py --command disable

    # Step 2: Perform the actual upgrade:
    $ aptitude update
    $ aptitude upgrade

    # Do not forget to review changes in configuration files/structure and
    # adapt accordingly.

    # Step 3: To be safe activate the Python virtual environment for Mentat system:
    $ . /var/mentat/venv/bin/activate

    # Step 4: Make sure your database schema is up to date. Please be aware, that
    # these operations may need a lot of time to complete depending on the size
    # of your database:
    (venv) $ time hawat-cli db upgrade
    (venv) $ time /etc/mentat/scripts/sqldb-migrate.sh upgrade head
    (venv) $ time /etc/mentat/scripts/sqldb-optimize.sh
    # At this point it could be wise to verify the state of the database and
    # perform some maintenance tasks to prevent from thrashing. Please be aware,
    # that these operations may need a lot of time to complete depending on the
    # size of your database:
    (venv) $ psql mentat_events
    \timing on
    VACUUM FREEZE VERBOSE;
    CLUSTER VERBOSE;
    ANALYZE VERBOSE;
    (venv) $ psql mentat_main
    \timing on
    VACUUM FREEZE VERBOSE;
    CLUSTER VERBOSE;
    ANALYZE VERBOSE;

    # Step 5: Deactivate now unnecessary virtual environment:
    (venv) $ deactivate

    # Step 6: Start all your Mentat daemons and cronjobs again:
    $ mentat-controller.py --command start
    $ mentat-controller.py --command enable

    # Step 7: Restart the web server that is serving web interface:
    $ a2dismod substitute
    $ a2dissite site_maintenance.conf
    $ a2ensite site_mentat-ng.conf
    $ systemctl restart apache2

For your convenience there is a ``maintenance-mode.sh`` script that you can use
to quickly turn maintenance mode ON/OFF. Upgrading steps are then much more simple:

.. code-block:: shell

    # Launch tmux or screen.
    tmux

    # Step 1: Activate maintenance mode:
    $ /etc/mentat/scripts/maintenance-mode.sh on

    # Now perform steps 2-5 from the checklist above

    # Step 6: Deactivate maintenance mode:
    $ /etc/mentat/scripts/maintenance-mode.sh off


.. _section-upgrading-mentat-2-8:

Upgrading to Mentat 2.8
--------------------------------------------------------------------------------

* Rulesets for two inspectors have been merged in to one instance. That
  concerns mentat-inspector.py, mentat-inspector-b.py.conf and
  mentat-controller.py.conf. If you use vanilla configuration from .deb,
  package manager will replace the old configuration files with new ones
  automatically. If you used different installation method or made some
  modifications into these configuration files, you are encouraged to review
  the changes and decide whether you want to stay with split configuration
  or also proceed with the merge.


.. _section-upgrading-postgresql-10:

Upgrading PostgreSQL from 10.x to 11.x
--------------------------------------------------------------------------------

Following checklist describes the steps necessary to upgrade the PostgreSQL database
from version ``10.x`` to ``11.x``.

.. warning::

    Please be aware, that the database upgrade is NOT a straightforward operation.
    It can take a lot of time depending on the size of the current database,
    because the data files need to be converted to new format.

.. code-block:: shell

    # Launch tmux or screen.
    tmux

    # Step 0: Activate maintenance mode:
    # First update timestamps of maintenance start and maintenance end:
    $ vim /etc/mentat/apache/maintenance/.htaccess
    # Now bring the Mentat system web interface down and maintenance site up:
    $ a2enmod substitute
    $ a2dissite site_mentat-ng.conf
    $ a2ensite site_maintenance.conf
    $ systemctl restart apache2

    # Step 1: Stop all processes touching the PostgreSQL database:
    $ systemctl stop apache2
    $ mentat-controller.py --command stop
    $ mentat-controller.py --command disable
    $ systemctl stop postgresql

    # Step 2: Install PostgreSQL 11:
    $ aptitude update
    $ aptitude install postgresql-11 postgresql-11-ip4r postgresql-server-dev-11 postgresql-client-11

    # Step 3: Verify the installation success (output included):
    $ pg_lsclusters
    :Ver Cluster Port Status Owner    Data directory              Log file
    :10  main    5432 online postgres /var/lib/postgresql/10/main /var/log/postgresql/postgresql-10-main.log
    :11  main    5433 online postgres /var/lib/postgresql/11/main /var/log/postgresql/postgresql-11-main.log

    # Step 4: PostgreSQL was started during installation, stop it again:
    $ systemctl stop postgresql

    # Step 5: Drop the default PostgreSQL 11 cluster created during installation:
    $ pg_dropcluster 11 main

    # Step 6: Verify the clusters (output included):
    $ pg_lsclusters
    :Ver Cluster Port Status Owner    Data directory              Log file
    :10  main    5432 down   postgres /var/lib/postgresql/10/main /var/log/postgresql/postgresql-10-main.log

    # Step 7: Perform the data migration (slow to complete):
    $ pg_upgradecluster --method=upgrade 10 main

    # Step 8: Drop the PostgreSQL 10 data as there are two copies (10+11):
    $ pg_dropcluster --stop 10 main

    # Step 9: Remove the old PostgreSQL version:
    $ aptitude purge postgresql-10 postgresql-10-ip4r postgresql-server-dev-10 postgresql-client-10

    # Step 10: Start the DB (maintenance still required, not ready for system uptime):
    $ systemctl start postgresql

    # Step 11: From the PostgreSQL shell (psql):
    # The CLUSTER is optional, it takes time but can shrink the DB size considerably if not done recently
    VACUUM VERBOSE;
    -- CLUSTER VERBOSE events;
    ANALYZE VERBOSE;

    # Step 12: This is a good time for restart (optional). New kernel? Long uptime & non-ECC RAM?
    $ reboot

    # Step 13: Now the system is ready for production, start it up
    $ systemctl restart postgresql
    $ systemctl start apache2
    $ mentat-controller.py --command enable
    $ mentat-controller.py --command start

    # Step 14: Restart the web server that is serving web interface:
    $ a2dismod substitute
    $ a2dissite site_maintenance.conf
    $ a2ensite site_mentat-ng.conf
    $ systemctl restart apache2

After these steps it is necessary to update following configuration files:

``/etc/mentat/mentat-cleanup.py.conf``
    Change configuration ``db_path`` to point to correct filesystem location. In default
    Debian installations it should look something like this:

    ``"db_path": "/var/lib/postgresql/11/main",``


.. _section-upgrading-postgresql-11:

Upgrading PostgreSQL from 11.x to 12.x
--------------------------------------------------------------------------------

Following checklist describes the steps necessary to upgrade the PostgreSQL database
from version ``11.x`` to ``12.x``.

.. warning::

    Please be aware, that the database upgrade is NOT a straightforward operation.
    It can take a lot of time depending on the size of the current database,
    because the data files need to be converted to new format.

    Upgrade to the latest version of Mentat prior to upgrading PostgreSQL.

.. code-block:: shell

    # Launch tmux or screen.
    tmux

    # Step 0: Activate maintenance mode:
    # First update timestamps of maintenance start and maintenance end:
    $ vim /etc/mentat/apache/maintenance/.htaccess
    # Now bring the Mentat system web interface down and maintenance site up:
    $ a2enmod substitute
    $ a2dissite site_mentat-ng.conf
    $ a2ensite site_maintenance.conf
    $ systemctl restart apache2

    # Step 1: Stop all processes touching the PostgreSQL database:
    $ sudo systemctl stop warden_filer_cesnet_receiver.service
    $ sudo systemctl disable warden_filer_cesnet_receiver.service
    $ sudo mentat-controller.py --command stop
    $ sudo mentat-controller.py --command disable
    $ systemctl restart postgresql

    ### There can be no DB writes beyond this point as we are about to drop indices to ensure data integrity!

    # Step 2: Connect to current database:
    $ psql mentat_events
    DROP INDEX events_detecttime_idx;
    DROP INDEX events_combined_idx;
    DROP INDEX events_storagetime_idx;
    DROP INDEX events_eventseverity_idx;
    ALTER TABLE events DROP CONSTRAINT events_pkey;
    VACUUM FREEZE VERBOSE;
    CHECKPOINT;

    # Step 3: Stop PostgreSQL:
    $ sudo systemctl stop postgresql

    # Step 4: Install PostgreSQL 12:
    $ sudo apt-get update
    $ sudo apt-get install postgresql-12 postgresql-12-ip4r postgresql-server-dev-12 postgresql-client-12

    # Step 5: Migration:
    $ sudo pg_lsclusters
    Ver Cluster Port Status Owner    Data directory              Log file
    11  main    5432 online postgres /var/lib/postgresql/11/main /var/log/postgresql/postgresql-11-main.log
    12  main    5433 online postgres /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log

    $ sudo systemctl stop postgresql

    $ sudo pg_dropcluster 12 main

    $ sudo pg_lsclusters
    Ver Cluster Port Status Owner    Data directory              Log file
    11  main    5432 down   postgres /var/lib/postgresql/11/main /var/log/postgresql/postgresql-11-main.log

    # This will require *temporarily* setting wal_level to 'logical' (in postgresql.conf) - it is set to 'minimal' if you followed configuration advice from docs
    # Alternatively one can ommit the --link parameter, but that requires free space for a 1:1 copy and of course also takes much longer
    $ sudo pg_upgradecluster --method=upgrade --link 11 main

    $ sudo pg_dropcluster 11 main

    # Step 6: Remove PostgreSQL 11 and all prior versions:
    $ sudo apt-get remove --purge postgresql-11 postgresql-client-11 postgresql-server-dev-11 postgresql-11-ip4r postgresql-9.4 postgresql-9.5 postgresql-9.6 postgresql-10

    # Step 7: Start PostgreSQL:
    $ sudo systemctl start postgresql

    # Step 8: Recreate indices:
    REINDEX DATABASE mentat_events;
    ALTER TABLE events ADD PRIMARY KEY (id);
    CREATE INDEX IF NOT EXISTS events_detecttime_idx ON events USING BTREE (detecttime);
    CREATE INDEX IF NOT EXISTS events_storagetime_idx ON events USING BTREE (storagetime);
    CREATE INDEX IF NOT EXISTS events_eventseverity_idx ON events USING BTREE (eventseverity) WHERE eventseverity IS NOT NULL;
    CREATE INDEX IF NOT EXISTS events_combined_idx ON events USING GIN (category, node_name, protocol, source_port, target_port, source_type, target_type, node_type, resolvedabuses, inspectionerrors);
    CHECKPOINT;
    ANALYZE VERBOSE;

    # Step 9: This is a good time for restart (optional). New kernel? Long uptime & non-ECC RAM?
    $ reboot

    # Step 10: Start Mentat and all other services:
    $ systemctl restart postgresql
    $ sudo mentat-controller.py --command enable
    $ sudo mentat-controller.py --command start
    $ sudo systemctl start warden_filer_cesnet_receiver.service
    $ sudo systemctl enable warden_filer_cesnet_receiver.service

    # Step 11: Restart the web server that is serving web interface:
    $ a2dismod substitute
    $ a2dissite site_maintenance.conf
    $ a2ensite site_mentat-ng.conf
    $ systemctl restart apache2


.. _section-upgrading-postgresql-12:

Upgrading PostgreSQL from 12.x to 13.x
--------------------------------------------------------------------------------

Following checklist describes the steps necessary to upgrade the PostgreSQL database
from version ``12.x`` to ``13.x``.

.. warning::

    Please be aware, that the database upgrade is NOT a straightforward operation.
    It can take a lot of time depending on the size of the current database,
    because the data files need to be converted to new format.

    Upgrade to the latest version of Mentat prior to upgrading PostgreSQL.

.. code-block:: shell

    # Launch tmux or screen.
    tmux

    # Step 0: Activate maintenance mode:
    # First update timestamps of maintenance start and maintenance end:
    $ vim /etc/mentat/apache/maintenance/.htaccess
    # Now bring the Mentat system web interface down and maintenance site up:
    $ a2enmod substitute
    $ a2dissite site_mentat-ng.conf
    $ a2ensite site_maintenance.conf
    $ systemctl restart apache2

    # Step 1: Stop all processes touching the PostgreSQL database:
    $ sudo systemctl stop warden_filer_cesnet_receiver.service
    $ sudo systemctl disable warden_filer_cesnet_receiver.service
    $ sudo mentat-controller.py --command stop
    $ sudo mentat-controller.py --command disable
    # Make sure there are no open or stale transactions or maintenance running
    $ systemctl restart postgresql

    ### There must be no DB writes beyond this point as we are about to drop indices to ensure data integrity!

    # Step 2: Connect to current database:
    $ psql mentat_events
    DROP INDEX events_detecttime_idx;
    DROP INDEX events_combined_idx;
    DROP INDEX events_storagetime_idx;
    DROP INDEX events_eventseverity_idx;
    ALTER TABLE events_json DROP CONSTRAINT events_json_id_fkey;
    ALTER TABLE events_json DROP CONSTRAINT events_json_pkey;
    ALTER TABLE events DROP CONSTRAINT events_pkey;
    VACUUM FREEZE;
    CHECKPOINT;

    # Step 3: Stop PostgreSQL:
    $ sudo systemctl stop postgresql

    # Step 4: Install PostgreSQL 13:
    $ sudo apt-get update
    $ sudo apt-get install postgresql-13 postgresql-13-ip4r postgresql-server-dev-13 postgresql-client-13

    # Step 5: Back up the default PostgreSQL v13 configuration file
    # This is used later in step 9.
    $ cp /etc/postgresql/13/main/postgresql.conf ~/postgresql_13_default.conf

    # Step 6: Migration:
    $ sudo pg_lsclusters
    Ver Cluster Port Status Owner    Data directory              Log file
    12  main    5432 online postgres /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log
    13  main    5433 online postgres /var/lib/postgresql/13/main /var/log/postgresql/postgresql-13-main.log

    $ sudo systemctl stop postgresql

    $ sudo pg_dropcluster 13 main

    $ sudo pg_lsclusters
    $ sudo pg_lsclusters
    Ver Cluster Port Status Owner    Data directory              Log file
    12  main    5432 down   postgres /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log

    # Change wal_level to 'logical' (in postgresql.conf) if it is se to 'minimal' (which should, if you followed configuration advice from docs).
    # This is *temporary* change for migration.
    # Alternatively one can ommit the --link argument, but that requires free space for a 1:1 copy and of course also takes much longer
    $ sudo pg_upgradecluster --method=upgrade --link 12 main

    $ sudo pg_dropcluster 12 main

    # Step 7: Remove PostgreSQL 12 and all prior versions:
    $ $ sudo apt-get remove --purge postgresql-12 postgresql-client-12 postgresql-server-dev-12 postgresql-12-ip4r postgresql-11 postgresql-client-11 postgresql-server-dev-11 postgresql-11-ip4r postgresql-10 postgresql-9.4 postgresql-9.5 postgresql-9.6

    # Step 8: Update the configuration file
    # This is the most laborous step, which I have found no way of automating. Also, rarely the options are just reordered, which complicates the merge process.
    $ sudo vimdiff /etc/postgresql/13/main/postgresql.conf ~/postgresql_13_default.conf

    # Change the following options in /etc/postgresql/13/main/postgresql.conf:
    autovacuum_vacuum_insert_threshold = -1

    # Change the setting for wal_level back to 'minimal' if it was changed in step 7.

    # Step 9a: Reboot the system:
    # OPTIONAL: This is a good time to reboot the machine if desired (kernel update, long uptime & non-ECC RAM). Alternatively, just follow with 9b.
    $ sudo reboot

    # Step 9b: Start PostgreSQL:
    # Only if 9a was skipped.
    $ sudo systemctl start postgresql

    # Step 10: Recreate indices:
    # psql mentat_events
    ANALYZE;
    REINDEX DATABASE mentat_events;
    ALTER TABLE events ADD PRIMARY KEY (id);
    ALTER TABLE events_json ADD PRIMARY KEY (id);
    ALTER TABLE events_json ADD FOREIGN KEY (id) REFERENCES events(id) ON DELETE CASCADE;
    CREATE INDEX IF NOT EXISTS events_detecttime_idx ON events USING BTREE (detecttime);
    CREATE INDEX IF NOT EXISTS events_storagetime_idx ON events USING BTREE (storagetime);
    CREATE INDEX IF NOT EXISTS events_eventseverity_idx ON events USING BTREE (eventseverity) WHERE eventseverity IS NOT NULL;
    CREATE INDEX IF NOT EXISTS events_combined_idx ON events USING GIN (category, node_name, protocol, source_port, target_port, source_type, target_type, node_type, resolvedabuses, inspectionerrors);
    CREATE INDEX IF NOT EXISTS events_ip_aggr_idx ON events USING GIST (source_ip_aggr_ip4, target_ip_aggr_ip4, source_ip_aggr_ip6, target_ip_aggr_ip6);
    CHECKPOINT;

    # Step 11: Start Mentat and all other services:
    $ systemctl restart postgresql
    $ sudo mentat-controller.py --command enable
    $ sudo mentat-controller.py --command start
    $ sudo systemctl start warden_filer_cesnet_receiver.service
    $ sudo systemctl enable warden_filer_cesnet_receiver.service

    # Step 12: Deactivate maintenance mode and restart the web server that is serving web interface:
    $ a2dismod substitute
    $ a2dissite site_maintenance.conf
    $ a2ensite site_mentat-ng.conf
    $ systemctl restart apache2


.. _section-upgrading-geoip:

Upgrading to authenticated version of GeoIP service
--------------------------------------------------------------------------------

Since 30.12.2019 the `MaxMind <https://www.maxmind.com/en/home>`__ IP geolocation
service  `changed significantly <https://blog.maxmind.com/2019/12/18/significant-changes-to-accessing-and-using-geolite2-databases/>`__
its access policies for downloading free versions of IP geolocation databases.
These

Following actions are necessary to make event enrichment with IP geolocation data
working again:

1. Follow installation instructions in section :ref:`section-installation-prerequisites-geoipupdate`.
2. Upgrade to Mentat system version ``2.6.x`` and review all default configuration file changes. The changes occured in ``/etc/mentat/mentat-controller.py.conf`` and ``/etc/mentat/core/services.json.conf`` configuration files. If you are reading this before Mentat version ``2.6.x`` is released please make following configuration changes manually:

.. code-block:: shell

    diff --git a/conf/mentat-controller.py.conf b/conf/mentat-controller.py.conf
    index 493903e2..4e454073 100644
    --- a/conf/mentat-controller.py.conf
    +++ b/conf/mentat-controller.py.conf
    @@ -134,7 +134,7 @@
             { "name": "mentat-cleanup-py" },

    -        # Utility for fetching current versions of IP geolocation databases.
    -        { "name": "fetch-geoipdb-sh" },
    -
             # Provide periodical informational report emails about overall performance of Mentat system.
             { "name": "mentat-informant-py" },

    diff --git a/conf/core/services.json.conf b/conf/core/services.json.conf
    index 81988e55..ca38cdba 100644
    --- a/conf/core/services.json.conf
    +++ b/conf/core/services.json.conf
    @@ -25,9 +25,9 @@
             # GeoIP service settings.
             #
             "geoip": {
    -            "asndb": "/var/opt/opensourcedbs/GeoLite2-ASN.mmdb",
    -            "citydb": "/var/opt/opensourcedbs/GeoLite2-City.mmdb"
    -            #"countrydb": "/var/opt/opensourcedbs/GeoLite2-Country.mmdb"
    +            "asndb": "/usr/share/GeoIP/GeoLite2-ASN.mmdb",
    +            "citydb": "/usr/share/GeoIP/GeoLite2-City.mmdb"
    +            #"countrydb": "/usr/share/GeoIP/GeoLite2-Country.mmdb"
             },

3. Restart Mentat system:

.. code-block:: shell

    $ mentat-controller.py --command stop
    $ mentat-controller.py --command start
    $ mentat-controller.py --command enable
    $ systemctl restart apache2

4. Finally make sure all obsolete and deprecate files are gone:

.. code-block:: shell

    rm -f /etc/cron.d/fetch-geoipdb-sh
    rm -f /etc/mentat/cron/fetch-geoipdb-sh.cron
    rm -f /etc/mentat/scripts/fetch-geoipdb.sh


.. _section-upgrading-debian:

Upgrading underlying Debian system
--------------------------------------------------------------------------------

After upgrading underlying Debian system please execute following set of commands
to repair installation of Mentat system:

.. code-block:: shell

    $ rm -rf /var/mentat/venv
    $ apt install mentat-ng --reinstall


.. _section-upgrading-next:

What is next?
--------------------------------------------------------------------------------

You have just successfully upgraded Mentat system to latest version, so what is
next?

* If you want to check what is new or got fixed in Mentat system, you might wish
  to check the `roadmap for appropriate version <https://homeproj.cesnet.cz/projects/mentat/roadmap>`__.
