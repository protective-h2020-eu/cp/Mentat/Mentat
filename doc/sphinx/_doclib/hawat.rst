.. _section-hawat:

Hawat: Web user interface
================================================================================

Hawat is a default web user interface for the Mentat system. It is implemented
using excelent lightweight `Flask <https://palletsprojects.com/p/flask/>`__
microframework with emphasis on modularity. Each module is implemented as
a standalone plugin that can be easily enabled by configuration.


.. _section-hawat-plugins:

Plugins
--------------------------------------------------------------------------------

Following is a list of all currently available Hawat plugins:

.. toctree::
    :glob:
    :maxdepth: 1

    hawat_plugin_*


.. _section-hawat-configuration:

Configuration
--------------------------------------------------------------------------------

Hawat provides very flexible configuration system to be as much customizable as
possible. The most important is your local version of ``/etc/mentat/mentat-hawat.py``
configuration file. In this file you may tweak the default configuration according
to your preferences. Fillowing is a non exhaustive list of the most important
configuration options:

.. code-block::

	# Force the debug mode.
	#DEBUG = True

	# Move the application from webserver root to different location. This setting
	# is crucial for generating correct inter-application links.
	APPLICATION_ROOT = '/mentat/'

	# Setup local secret key to be used as cryptographic salt.
	SECRET_KEY = 'you-should-really-change-this-asap'

	# Define list of Hawat administrator emails. These users will receive notifications
	# about application errors, account creations etc. In this example local user
	# 'mentat-admin' will be used and local file '/etc/aliases' will contain
	# specific email addresses.
	HAWAT_ADMINS = ['mentat-admin']

	# Define list of Hawat administrator emails, that receive feedback messages for reports.
	# These users will receive report feedback emails. In this example local user
	# 'mentat-admin' will be used and local file '/etc/aliases' will contain
	# specific email addresses.
	HAWAT_REPORT_FEEDBACK_MAILS = ['mentat-admin']

	# Directories with translations used in Hawat.
	BABEL_TRANSLATION_DIRECTORIES = 'translations;/etc/mentat/templates/reporter/translations'

	# Localization settings.
	#BABEL_DEFAULT_LOCALE   = 'en'
	BABEL_DEFAULT_LOCALE    = 'cs'
	#BABEL_DEFAULT_TIMEZONE = 'UTC'
	BABEL_DEFAULT_TIMEZONE  = 'Europe/Prague'

	# List of enabled application blueprints. Tweak this list according to your preferences.
	# Please be aware that there might be dependencies among the modules and some
	# modules are de-facto mandatory (design, home, ...).
	ENABLED_BLUEPRINTS = [
	     'hawat.blueprints.auth_api',
	     'hawat.blueprints.auth_env',
	     'hawat.blueprints.design',
	     'hawat.blueprints.home',
	     'hawat.blueprints.reports',
	     'hawat.blueprints.events',
	     'hawat.blueprints.hosts',
	     'hawat.blueprints.timeline',
	     'hawat.blueprints.dnsr',
	     'hawat.blueprints.pdnsr',
	     'hawat.blueprints.geoip',
	     'hawat.blueprints.nerd',
	     'hawat.blueprints.whois',
	     'hawat.blueprints.performance',
	     'hawat.blueprints.status',
	     'hawat.blueprints.dbstatus',
	     'hawat.blueprints.devtools',
	     'hawat.blueprints.users',
	     'hawat.blueprints.groups',
	     'hawat.blueprints.settings_reporting',
	     'hawat.blueprints.filters',
	     'hawat.blueprints.networks',
	     'hawat.blueprints.changelogs',
	]


.. _section-hawat-webapi:

Web API
--------------------------------------------------------------------------------

The API interface must be accessed by authenticated user. For web client side scripts
and applications it should be sufficient to use standard cookie-based authentication.

If you need to access the API from outside of the web browser, it might be usefull
to generate yourself an API access key and use the key based authentication.
Please refer to section :ref:`section <section-hawat-plugin-auth-api>` for more
details on API key base authentication service.

After that you have two options for using that key:

* Use ``POST`` method and send your key as ``api_key`` or ``api_token`` parameter.
  The ``POST`` method is necessary, otherwise the key might get logged on many
  different and insecure places (like web server logs).
* Use the ``Authorization`` HTTP header to submit the key as follows::

    Authorization: abcd1234
    Authorization: key abcd1234
    Authorization: token abcd1234

**Example usage with curl:**

.. code-block:: shell

    $ curl -X POST -d "api_key=your%AP1_k3y" "https://.../api/events/search?submit=Search"

**List of all currently known API endpoints:**

* :ref:`section-hawat-plugin-events`

	* :ref:`section-hawat-plugin-events-webapi-search`
	* :ref:`section-hawat-plugin-events-webapi-show`
	* :ref:`section-hawat-plugin-events-webapi-dashboard`
	* :ref:`section-hawat-plugin-events-webapi-metadata`
