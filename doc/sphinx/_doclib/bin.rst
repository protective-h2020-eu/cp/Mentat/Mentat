.. _section-bin-mentat:

System modules
================================================================================


.. _section-bin-rt:

Daemon modules (real-time message processing modules)
--------------------------------------------------------------------------------

Following is a list of all currently available real-time message processing modules.
These modules are implemented as daemons (or system services), which are always
running.

.. toctree::
    :maxdepth: 1

    bin_mentat-enricher
    bin_mentat-inspector
    bin_mentat-sampler
    bin_mentat-storage


.. _section-bin-pp:

Script modules (message post-processing modules)
--------------------------------------------------------------------------------

Following is a list of all currently available message post-processing modules.
These modules are implemented as scripts and are intended to be executed either
at regular intervals by cron-like system service, or ad-hoc from terminal by user.

.. toctree::
    :maxdepth: 1

    bin_mentat-statistician
    bin_mentat-informant
    bin_mentat-reporter


.. _section-bin-mm:

Utility and management modules
--------------------------------------------------------------------------------

Following is a list of all currently available utility and management modules.
These modules are implemented as scripts and are intended to be executed either
at regular intervals by cron-like system service, or ad-hoc from terminal by user.

.. toctree::
    :maxdepth: 1

    bin_mentat-backup
    bin_mentat-cleanup
    bin_mentat-controller
    bin_mentat-dbmngr
    bin_mentat-ideagen
    bin_mentat-netmngr


.. _section-bin-ui:

User interfaces
--------------------------------------------------------------------------------

Following is a list of all currently available user interfaces.

.. toctree::
    :maxdepth: 1

    bin_hawat-cli
    hawat
