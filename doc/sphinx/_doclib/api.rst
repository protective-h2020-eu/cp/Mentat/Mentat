.. _section-api:

API documentation
================================================================================

Python submodules
--------------------------------------------------------------------------------

.. toctree::
    :glob:
    :maxdepth: 1

    apidoc/*
