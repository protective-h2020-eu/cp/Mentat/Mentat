.. _section-quickstart:

Quickstart
================================================================================


Mentat backend
--------------------------------------------------------------------------------

First thing you can do after successfull :ref:`installation <section-installation>`
is to check the status of all Mentat system modules:

.. code-block:: shell

    # The 'status' command is default, so this is a short form:
    mentat-controller.py

    # And the same, but more verbose:
    mentat-controller.py --command status

    # Or even:
    mentat-controller.py --command=status

All Mentat modules display help when executed with proper option:

.. code-block:: shell

    # Most interesting Mentat modules for the beginning:
    mentat-controller.py --help
    mentat-inspector.py --help
    mentat-enricher --help
    mentat-storage --help
    mentat-cleanup.py --help
    mentat-ideagen.py --help
    mentat-statistician --help
    ...

Each Mentat module does have appropriate configuration file in ``/etc/mentat``
directory. You might want to browse through those and tweak something here and there.
The configuration file has the same name as the module itself with ``.conf`` suffix.

Let`s start things up:

.. code-block:: shell

    # Start real-time message processing modules:
    mentat-controller.py --command start

    # Enable cron-based message post-processing modules:
    mentat-controller.py --command enable

At this point all default Mentat modules should be running. You can verify that
fact by listing processes, tailing log files, or using the status command introduced
above:

.. code-block:: shell

    # List all Mentat related processes:
    ps aux | grep mentat

    # Tail log files of Mentat real-time message processing modules:
    tail -f /var/mentat/log/mentat-inspector.py.log
    tail -f /var/mentat/log/mentat-enricher.py.log
    tail -f /var/mentat/log/mentat-storage.py.log

    # Check the status with Mentat control script:
    mentat-controller.py

Now it is time to generate some dummy test data. Use following command to generate
some randomized `IDEA <https://idea.cesnet.cz/en/index>`__ messages and let the
Mentat modules process them:

.. code-block:: shell

    # Generate 1000 random test IDEA messages:
    mentat-ideagen.py --count 1000

Now verify, that `IDEA <https://idea.cesnet.cz/en/index>`__ messages indeed ended
up in database:

.. code-block:: shell

    su postgres
    psql mentat_events
    SELECT * FROM events;
    SELECT count(*) FROM events;

You may want to check the log files again and look for the signs of messages being
processed there. For example for the :ref:`section-bin-mentat-storage` module you
might see output like this:

.. code-block:: shell

    tail -f /var/mentat/log/mentat-storage.py.log
    2018-07-09 20:45:19,812 mentat-storage.py [8660] INFO: Component 'filer': *** Processing statistics ***
        cnt_enqueued                0 (+       0,     0.00 #/s)
        cnt_fetched           751,224 (+     396,    18.96 #/s)
        cnt_updated                 0 (+       0,     0.00 #/s)
        cnt_committed         751,224 (+     396,    18.96 #/s)
        cnt_banished                0 (+       0,     0.00 #/s)
        cnt_canceled                0 (+       0,     0.00 #/s)
        cnt_dispatched              0 (+       0,     0.00 #/s)
        cnt_duplicated              0 (+       0,     0.00 #/s)
        cnt_errors                  0 (+       0,     0.00 #/s)
    2018-07-09 20:45:19,813 mentat-storage.py [8660] INFO: Component 'parser': *** Processing statistics ***
        cnt_parsed            751,224 (+     396,    18.96 #/s)
        cnt_encoded                 0 (+       0,     0.00 #/s)
        cnt_errors                  0 (+       0,     0.00 #/s)
    2018-07-09 20:45:19,813 mentat-storage.py [8660] INFO: Component 'storage': *** Processing statistics ***
        cnt_stored            751,224 (+     396,    18.96 #/s)
        cnt_errors                  0 (+       0,     0.00 #/s)
    2018-07-09 20:45:19,813 mentat-storage.py [8660] INFO: Waiting for '7' seconds until next scheduled event
    2018-07-09 20:45:26,813 mentat-storage.py [8660] INFO: Received wakeup signal (14)
    2018-07-09 20:45:26,813 mentat-storage.py [8660] INFO: Waiting for '2' seconds until next scheduled event
    2018-07-09 20:45:28,814 mentat-storage.py [8660] INFO: Received wakeup signal (14)
    2018-07-09 20:45:28,815 mentat-storage.py [8660] INFO: Component 'filer': Fetched message 'mentat.66767.1531161912.232503.2051.58991861.idea'
    2018-07-09 20:45:28,816 mentat-storage.py [8660] INFO: Component 'parser': Parsed IDEA message '93cae7e0-ff84-476c-87fd-446e3997d855' from message file 'mentat.66767.1531161912.232503.2051.58991861.idea'


Mentat web interface
--------------------------------------------------------------------------------

Mentat ships with built-in web user interface called Hawat. Point your web browser
to the correct URL and you should see something similar to this (depending on the
size of your viewport you may see more or less information):

.. image:: /_static/mentat-hawat-index.png

You have to login using the dropdown button labeled *Anonymous* in the top right
corner. You may switch to more appropriate locale, if that is available in your
installation. If you are browsing through demo installation try following combinations
of login nad password::

    admin      -> admin
    maintainer -> maintainer
    developer  -> developer
    user       -> user

After successfull login the main menu bar at the top of the page appears and you
may proceed. Following figures are appropriate for **admin** level user account.


Module: Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The **Events** module provides interface for accessing and searching the
`IDEA <https://idea.cesnet.cz/en/index>`__ event database and inspecting the event
details. The main search page should look similar to this image:

.. image:: /_static/mentat-hawat-events-search-form.png

Use to search form to give the parameters for your search query. Use the buttons
on top of the form to toggle on/off the search option groups. You may also leave
the search form empty and just hit **Search**. Search result page should look
similar to this image:

.. image:: /_static/mentat-hawat-events-search-result.png

You may inspect the datails of particular event by clicking the **View** icon in
actions table column and the event detail page should look similar to this image:

.. image:: /_static/mentat-hawat-events-detail.png
