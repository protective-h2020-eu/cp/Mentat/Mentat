{{ _('Dear administrator,') | wordwrap }}

{{ _('a new account "%(item_id)s" was just registered in %(app_name)s. Please review the following request and activate or delete the account:', item_id = account.login, app_name = vial_appname) | wordwrap }}

    {{ '{:16s}'.format(_('Login:')) }} {{ account.login }}
    {{ '{:16s}'.format(_('Full name:')) }} {{ account.fullname }}
    {{ '{:16s}'.format(_('Email:')) }} {{ account.email }}
    {{ '{:16s}'.format(_('Organization:')) }} {{ account.organization }}
{%- if account.memberships_wanted %}

{{ _('User has requested membership in following groups:') | wordwrap }}
{%- for group in account.memberships_wanted %}
 * {{ group.name }}
{%- endfor %}
{%- endif %}

{{ _('User has provided following justification to be given access to the system:') | wordwrap }}

{{ justification | wordwrap(width=75, break_long_words=False) | indent(width=4, first=True) }}

{{ _('Account details can be found here:') | wordwrap }}

  {{ url_for('users.show', _external = True, item_id = account.id ) }}

{{ _('Have a nice day') | wordwrap }}

-- {{ vial_appname }}
