//------------------------------------------------------------------------------
// This file is part of Mentat system (https://mentat.cesnet.cz/).
//
// Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
// Author: Jan Mach <jan.mach@cesnet.cz>
// Use of this source is governed by the MIT license, see LICENSE file.
//------------------------------------------------------------------------------

module.exports = function(grunt) {

    function minify_json_file(src, tgt) {
        src_content = grunt.file.readJSON(src);
        tgt = tgt.replace(/\.json$/, '.min.json')
        grunt.file.write(
            tgt,
            JSON.stringify(src_content)
        );
        console.log("Minified JSON file '" + src + "' to '" + tgt + "'");
    }

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // Project paths to important directories.
        project_paths: {
            'web_dir':        'lib/hawat/',
            'web_static_dir': 'lib/hawat/static/'
        },

        // ---------------------------------------------------------------------
        // Cleanup various locations.
        clean: {
            // Cleanup web UI directory
            webui: {
                src: [
                    "<%= project_paths.web_static_dir %>vendor"
                ]
            }
        },

        // ---------------------------------------------------------------------
        // Running shell commands.
        shell: {
            // Compile language dictionaries.
            pybabel_hawat: {
                command: 'make hpybabel-compile'
            }
        },

        // ---------------------------------------------------------------------
        // Copy certain files to appropriate locations.
        copy: {
            // Copy components for web user interface.
            webui: {
                files: [
                    // ----- Popper.js
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/popper.js/dist/umd/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/popper.js/js/'
                    },
                    // ----- Bootstrap
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/bootstrap/dist/css/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/bootstrap/css/'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/bootstrap/dist/fonts/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/bootstrap/fonts/'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/bootstrap/dist/js/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/bootstrap/js/'
                    },
                    // ----- CLDRJS.
                    {
                        expand: true,
                        cwd: 'node_modules/cldrjs/dist/',
                        src: './cldr.js',
                        dest: '<%= project_paths.web_static_dir %>vendor/cldrjs'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/cldrjs/dist/cldr/',
                        src: './*.js',
                        dest: '<%= project_paths.web_static_dir %>vendor/cldrjs/cldr'
                    },
                    // ----- CLDR.
                    {
                        expand: true,
                        cwd: 'node_modules/cldr-data/main/cs/',
                        src: './*.json',
                        dest: '<%= project_paths.web_static_dir %>vendor/cldr/main/cs'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/cldr-data/main/en/',
                        src: './*.json',
                        dest: '<%= project_paths.web_static_dir %>vendor/cldr/main/en'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/cldr-data/supplemental/',
                        src: './*.json',
                        dest: '<%= project_paths.web_static_dir %>vendor/cldr/supplemental'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/iana-tz-data/',
                        src: './iana-tz-data.json',
                        dest: '<%= project_paths.web_static_dir %>vendor/iana-tz-data'
                    },
                    // ----- Globalize.
                    {
                        expand: true,
                        cwd: 'node_modules/globalize/dist/',
                        src: './globalize.js',
                        dest: '<%= project_paths.web_static_dir %>vendor/globalize'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/globalize/dist/globalize/',
                        src: './*.js',
                        dest: '<%= project_paths.web_static_dir %>vendor/globalize/globalize'
                    },
                    // ----- D3
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/d3/',
                        src: './d3*.js',
                        dest: '<%= project_paths.web_static_dir %>vendor/d3/js/'
                    },
                    // ----- NVD3
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/nvd3/build/',
                        src: './*.js*',
                        dest: '<%= project_paths.web_static_dir %>vendor/nvd3/js/'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/nvd3/build/',
                        src: './*.css*',
                        dest: '<%= project_paths.web_static_dir %>vendor/nvd3/css/'
                    },
                    // ----- Moment.JS
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/moment/min/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/moment/js/'
                    },
                    // ----- Bootstrap-Select
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/bootstrap-select/dist/css/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/bootstrap-select/css/'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/bootstrap-select/dist/js/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/bootstrap-select/js/'
                    },
                    // ----- eonasdan-bootstrap-datetimepicker
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/eonasdan-bootstrap-datetimepicker/build/css/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/bootstrap-datetimepicker/css/'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/eonasdan-bootstrap-datetimepicker/build/js/',
                        src: './*',
                        dest: '<%= project_paths.web_static_dir %>vendor/bootstrap-datetimepicker/js/'
                    },
                    // ----- Font-Awesome Free
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/@fortawesome/fontawesome-free/css/',
                        src: './**',
                        dest: '<%= project_paths.web_static_dir %>vendor/font-awesome/css/'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/@fortawesome/fontawesome-free/webfonts/',
                        src: './**',
                        dest: '<%= project_paths.web_static_dir %>vendor/font-awesome/webfonts/'
                    },
                    // ----- jQuery
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/jquery/dist/',
                        src: './jquery.min.js',
                        dest: '<%= project_paths.web_static_dir %>vendor/jquery/js/'
                    }
                ]
            }
        }
    });

    // ---------------------------------------------------------------------
    // Load grunt modules.
    require('load-grunt-tasks')(grunt, { scope: 'devDependencies' });
    require('time-grunt')(grunt);

    // Subtask for minification of CLDR JSON files.
    grunt.registerTask('minify-cldrs', function() {
        // Minify IANA timezone metadata.
        [
            [
                'node_modules/iana-tz-data/iana-tz-data.json',
                '<%= project_paths.web_dir %>templates/iana-tz-data/iana-tz-data.json'
            ],
            [
                'node_modules/iana-tz-data/iana-tz-data.json',
                '<%= project_paths.web_static_dir %>vendor/iana-tz-data/iana-tz-data.json'
            ]
        ].forEach(function(item) {
            src = grunt.template.process(item[0]);
            tgt = grunt.template.process(item[1]);
            minify_json_file(src, tgt);
        });

        // Minify CLDR metadata.
        [
            {
                expand: true,
                cwd: 'node_modules/cldr-data/main/cs/',
                src: './*.json',
                dest: '<%= project_paths.web_static_dir %>vendor/cldr/main/cs'
            },
            {
                expand: true,
                cwd: 'node_modules/cldr-data/main/en/',
                src: './*.json',
                dest: '<%= project_paths.web_static_dir %>vendor/cldr/main/en'
            },
            {
                expand: true,
                cwd: 'node_modules/cldr-data/supplemental/',
                src: './*.json',
                dest: '<%= project_paths.web_static_dir %>vendor/cldr/supplemental'
            }
        ].forEach(function(item) {
            file_mapping = grunt.file.expandMapping(item.src, item.dest, item);
            file_mapping.forEach(function(subitem) {
                src = grunt.template.process(subitem.src[0]);
                tgt = grunt.template.process(subitem.dest);
                minify_json_file(src, tgt);
            });
        });
    });

    // ---------------------------------------------------------------------
    // Setup custom task(s).
    // ---------------------------------------------------------------------

    grunt.registerTask(
        'webui',
        '(RUN) Build and install web user interface dependencies.',
        ['clean:webui', 'shell:pybabel_hawat', 'copy:webui', 'minify-cldrs']
    );
    grunt.registerTask(
        'default',
        '(RUN) Alias for webui.',
        ['webui']
    );
};
